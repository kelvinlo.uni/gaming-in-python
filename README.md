# Gaming in Python

Interactive games developed in Python (with jupyter notebook)

Below are the file descriptions:
<table>
  <thead>
    <td>File</td>
    <td>Description</td>
  </thead>
  <tr>
    <td>Monopoly.ipynb</td>
    <td>A simple and mini 4x4 Monopoly game developed in Python</td>
  </tr>
</table>